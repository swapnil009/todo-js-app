$(document).ready(function(){
  $('#user-reg').validate({
    rules:{
      'first_name': {
        required: true,
        maxlength: 20,
        minlength: 3
      },
      'last_name': {
        required: true,
        maxlength: 20,
        minlength: 3
      },
      'email': {
        required: true
      },
      "password": {
        required: true,
        validateStrength: true,
        minlength: 8
      }
    },
    submitHandler: function(form) {
      user.add($(form).serializeArray())
    }
  })

  resultCriteria = [];
  $.validator.addMethod("validateStrength", function(val, element) {
    resultCriteria = [];
    var re = /\d/;
    if (!re.test(val)) {
      resultCriteria.push("a number")
    }
    re = /[a-z]/;
    if (!re.test(val)) {
      resultCriteria.push("a lowercase letter")
    }
    re = /[A-Z]/;
    if (!re.test(val)) {
      resultCriteria.push("an uppercase letter")
    }
    re = /[$@$!%*?&]/;
    if (!re.test(val)) {
      resultCriteria.push("a symbol")
    }
    if (val.length < 8 ) {
      resultCriteria.push("a minimum of 8 characters")
    }

    $('#user_password_confirmation').removeClass('strongpass');

    if (resultCriteria.length != 0) {
      $(element).removeClass('strongpass');
      return false;
    } else {
      $(element).addClass('strongpass');
      return true;
    }
  }, function() {return 'It must have contain: ' + resultCriteria.join(', ')});
});