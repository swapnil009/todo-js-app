
  function appendMessage(id, removeString, addString, messageText){
    $(id).html('<div class="row message alert alert-success alert-dismissable" style="display: block"></div>')
    var closeBtn = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
    $(id).find("div").removeClass(removeString).addClass(addString).html(messageText + closeBtn)
    $(id).removeClass("hidden")
  }

  var serializeFormtoJson = function (form){
    var o = {}
    $.each(form, function () {
      if (o[this.name]) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  }

