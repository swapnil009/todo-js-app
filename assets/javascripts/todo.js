var todo = (function () {
  var itemsList = findItems()

  function addItem(form){
    appendItemInList(form)
    updateStorage()
    window.open("../todo/todo.html")
  }

  function search(form){
    var params = serializeFormtoJson(form)

    var fromDate = params.date_range.split('-')[0]

    var toDate = params.date_range.split('-')[1]
    
    if(params.date_range.length > 0){
      itemsList = itemsList.filter( function(item){
      return new Date(item.date).getTime() >= new Date(fromDate).getTime() &&
          new Date(item.date).getTime() <= new Date(toDate).getTime()
      });
    }

    if(params.category.length > 0){
      itemsList = itemsList.filter( function(item){
        return item.category == params.category
      });
    }

    if(params.is_pending == 'true'){
      itemsList = itemsList.filter( function(item){
        return item.is_pending == true
      });
    }

    if(params.is_done == 'true'){
      itemsList = itemsList.filter( function(item){
        return item.is_done == true
      });
    }
    sortItems(itemsList)
  }

  function newItem(){
    window.open("../todo/new_todo.html")
  }

  function updateStorage(){
    localStorage.setItem("items", JSON.stringify(itemsList))
  }

  function editItem(item_id){
    // var inputHtml = '<div class="form-group"><label for="todo_date">Date:</label><input type="text" name="date" class="form-control" id="todo_date_update" value=>'+item.date'</div>'
    var item = itemsList.find(findmatchedItem, item_id)
    localStorage.removeItem("edit_item")
    localStorage.setItem("edit_item",JSON.stringify(item))
    window.open('../todo/edit_todo.html')

  }

  function updateItem(form){
    var edit_item = JSON.parse(localStorage.getItem("edit_item"))
    var modified_item = Object.assign(edit_item, serializeFormtoJson(form))
    item_index = itemsList.indexOf(findmatchedItem, edit_item.id)
    itemList = itemsList.splice({id: edit_item.id}, 1)
    itemsList.push(modified_item)
    viewItem(edit_item.id.toString())
    updateStorage()
  }

  function viewItem(item_id){
    var item = itemsList.find(findmatchedItem, item_id)
    var html = '<p>Category'+item.category+'</p><p>Date'+item.date+'</p>'
    $('.container').html('')
    $('.container').append(html);

  }

  function allItems(){
    sortItems(itemsList)
  }

  function findItems(){
    var allItems = JSON.parse(localStorage.getItem('items'))
    return Array.isArray(allItems) ? allItems : []
  }

  function mergeIdWithItem(item){
    var length = itemsList.length
    var item_obj = new itemObj(item)
    return Object.assign({id: length + 1}, item_obj)
  }

  function appendItemInList(form) {
    var item = serializeFormtoJson(form)
    modified_item = mergeIdWithItem(item)
    itemsList.unshift(modified_item)
  }

  function findmatchedItem(item){
    return this[0] == item.id
  }

  function printItems(sorted_items){
    $('#items-list').html('')
    sorted_items.forEach(function(item){
      var html = '<tr><td>'+ item.date +'</td><td>'+ item.category + '</td><td>'+ item.reminder_date + '</td><td>'+item.is_pending+'</td><td>'+item.is_done+'</td><td>'+editBtn(item)+' | '+showBtn(item)+'</td></tr>'
      $('#items-list').append(html)
    });
  }

  function sortItems(items){
    var sorted_items = items.sort(function(a,b){
      return a.id - b.id;
    });
    printItems(sorted_items)
  }

  function editBtn(item){
    // return '<button class="btn btn-primary" onclick='+editItem(item.id)+'>Edit</button>'
    return '<input type="button" value="Edit" class="btn btn-primary" onClick="todo.editItem(\'' + item.id + '\')" />'
  }

  function showBtn(item){
    return '<input type="button" value="Show" class="btn btn-primary" onClick="todo.viewItem(\'' + item.id + '\')" />'
  }

  return {
    addItem: addItem,
    newItem: newItem,
    allItems: allItems,
    editItem: editItem,
    viewItem: viewItem,
    updateItem:updateItem,
    search: search
  }
})();


function itemObj(opts={}){
  this.date = opts['date'] || '';
  this.reminder_date = opts['reminder_date'] || '';
  this.category = opts['category'] || '' ;
  this.is_pending = true,
  this.is_public = opts['is_public']|| false
  this.is_done = false
}

function toggleReminderDate(el){
  if($(el).is(":checked")){
    $("input[name='reminder_date']").attr('disabled', false)
  }
  else{
    $("input[name='reminder_date']").attr('disabled', true)
  }
}

function toggleCheckbox(el){
  if($(el).is(":checked")){
      $(el).attr('value', 'true');
    } 
  else {
      $(el).attr('value', 'false');
  }
}

function togglependingCheckbox(el){
  if($(el).is(":checked")){
      $(el).attr('value', 'false');
    }
  else {
      $(el).attr('value', 'true');
  }
}

onDocumentReady =function(){
  $('#add-item-form').validate({
    rules:{
      'date': {
        required: true
      },
      'category': {
        required: true
      },
      'reminder_date': {
        required: true
      }
    },
    submitHandler: function(form) {
      todo.addItem($(form).serializeArray())
    }
  })


  $('#update-item-form').validate({
    rules:{
      'date': {
        required: true
      },
      'category': {
        required: true
      },
      'reminder_date': {
        required: true
      }
    },
    submitHandler: function(form) {
      todo.updateItem($(form).serializeArray())
    }
  })

  todo.allItems()

}

$(document).ready(onDocumentReady);