
//use this one handler for both Start and End Date pickers
var onDateSelect = function(selectedDate, input) {
  $("#todo_date, #reminder_date").datepicker('option', 'maxDate', selectedDate)
};
var onDocumentReady = function() {
  var datepickerConfiguration = {
    dateFormat: "dd/mm/yy",
    onSelect: onDateSelect
  };
  ///--- Component Binding ---///
  $('#todo_date, #reminder_date').datepicker(datepickerConfiguration);
  $('input[name="date_range"]').daterangepicker();
};
$(document).ready(onDocumentReady);