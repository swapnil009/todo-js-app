var user = (function () {
  var allUsers = findUsers()

  function add(form) {
    // Store
    appendUserInList(form)
    createUser()
  }

  function logIn(form){
    var user = serializeFormtoJson(form)
    var matchedUser = allUsers.filter(findMatchedUser, user)
    if(matchedUser.length > 0){
      localStorage.removeItem('items')
      window.open("../todo/todo.html")
    }
    else{
      appendMessage("#alert-msg","hidden","alert-danger in", "Enter Valid Credentials")
    }
  }

  function logOut(){
    window.close()
    window.open('../users/login.html')
  }

  // private functions
  function createUser(){
    localStorage.setItem("users", JSON.stringify(allUsers))
  }

  function mergeIdWithUser(user){
    var length = allUsers.length
    return Object.assign({id: length + 1}, user)
  }

  function findUsers(){
    var allUsers = JSON.parse(localStorage.getItem('users'))
    return Array.isArray(allUsers) ? allUsers : []
  }

  function findMatchedUser(user){
    return user.email == this.email && user.password == this.password
  }

  function appendUserInList(form) {
    var user = serializeFormtoJson(form)
    modified_user = mergeIdWithUser(user)
    allUsers.unshift(modified_user)
  }

  return {
    add: add,
    logIn: logIn,
    logOut
  }
})();